package main

import (
	"bitbucket.org/mblxa/TaskManager7/controllers/role"
	"bitbucket.org/mblxa/TaskManager7/controllers/task"
	"bitbucket.org/mblxa/TaskManager7/controllers/user"
	"bitbucket.org/mblxa/TaskManager7/database"
	"bitbucket.org/mblxa/TaskManager7/middleware/auth"
	"github.com/iris-contrib/middleware/cors"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/kataras/iris"
)

func main() {
	db := database.Init()
	defer db.Close()

	crs := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"}, // allows everything, use that to change the hosts.
		AllowCredentials: true,
		AllowedMethods:   []string{"OPTIONS", "GET", "POST", "DELETE"},
		AllowedHeaders:   []string{"Origin", "Content-Type", "Accept-Header", "Token"},
		Debug:            true,
	})

	app := iris.New()

	v1 := app.Party("/api/v1", crs, addGzip).AllowMethods(iris.MethodOptions)
	{
		v1.Post("/user/auth", userController.Auth)
		v1.Post("/user", userController.Create)

		roles := v1.Party("/roles", authMiddleware.CheckToken)
		{
			roles.Get("/", roleController.List)
			roles.Get("/{id:int}", roleController.Get)
			roles.Post("/", roleController.Create)
			roles.Delete("/{id:int}", roleController.Delete)
		}

		tasks := v1.Party("/tasks", authMiddleware.CheckToken)
		{
			tasks.Get("/", taskController.List)
			tasks.Get("/byDate", taskController.GetByDate)
			tasks.Get("/{id:int}", taskController.Get)
			tasks.Post("/change-status/{id:int}", taskController.ChangeStatus)
			tasks.Post("/", taskController.Create)
			tasks.Delete("/{id:int}", taskController.Delete)
		}

	}


	app.Run(iris.TLS(":5003", "fullchain.pem", "privkey.pem"))
	//serverSettings := settings.Settings.GetServerSettings()
	//if serverSettings.HTTPS {
//		app.Run(iris.TLS(serverSettings.IP+":"+serverSettings.PORT,
//			serverSettings.CERTIFICATE,
//			serverSettings.KEYFILE))
//	} else {
//		app.Run(iris.Addr(serverSettings.IP + ":" + serverSettings.PORT))
//	}

}

func addGzip(ctx iris.Context) {
	ctx.Gzip(true)
	ctx.Next()
}
