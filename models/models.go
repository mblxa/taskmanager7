package models

import (
	"bitbucket.org/mblxa/TaskManager7/passwords"
	"crypto/rand"
	"fmt"
	"github.com/jinzhu/gorm"
	"time"
)

type User struct {
	gorm.Model
	Name     string
	Email    string
	Password string
	Token    string
	Tasks    []Task
	Roles    []Role
}

func (u *User) BeforeCreate() (err error) {
	hashPassword, _ := passwords.HashPassword(u.Password, 1)
	u.Password = hashPassword
	return
}
func (u *User) GenerateToken() (err error) {
	b := make([]byte, 12)
	rand.Read(b)
	u.Token = fmt.Sprintf("%x", b)
	return
}

type Role struct {
	gorm.Model
	Name   string
	Color  string
	Tasks  []Task
	UserID uint `gorm:"index"`
}

const (
	StatusInProgress = "1"
	StatusComplete   = "2"
)

type Task struct {
	gorm.Model
	Name        string
	Description string
	Status      int
	Date        time.Time
	Duration    time.Duration
	Role        Role
	User        User
	UserID      uint `gorm:"index"`
	RoleID      uint `gorm:"index"`
}
