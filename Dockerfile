#FROM node:8.11.2-alpine as front
#WORKDIR /app
#COPY ./tm7front .
#RUN npm i npm@latest -g
#RUN npm install
#RUN npm run build-prod

FROM alpine:latest AS build
RUN apk update
RUN apk upgrade
RUN apk add --update go gcc g++
RUN apk add git
WORKDIR /app
ENV GOPATH /app
RUN go get github.com/kataras/iris
RUN go get github.com/jinzhu/gorm
RUN go get github.com/iris-contrib/middleware/cors
RUN go get golang.org/x/crypto/bcrypt
RUN go get github.com/jinzhu/gorm/dialects/sqlite
ADD . /app/src/bitbucket.org/mblxa/TaskManager7/
WORKDIR /app/src/bitbucket.org/mblxa/TaskManager7/
RUN CGO_ENABLED=1 GOOS=linux go build main.go

FROM nginx:1.13.12-alpine
WORKDIR /front
COPY ./assets /front
WORKDIR /app
RUN cd /app
COPY --from=build /app/src/bitbucket.org/mblxa/TaskManager7/main /app/main
COPY fullchain.pem .
COPY privkey.pem .
COPY start.sh .

COPY ./nginx.conf /etc/nginx/nginx.conf
#COPY --from=front /app/dist /app/assets

VOLUME ["/app/db"]
EXPOSE 80

CMD ["./start.sh"]