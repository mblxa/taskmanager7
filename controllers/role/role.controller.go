package roleController

import (
	"bitbucket.org/mblxa/TaskManager7/database"
	"bitbucket.org/mblxa/TaskManager7/models"
	"github.com/kataras/iris"
)

func List(ctx iris.Context) {
	user := ctx.Values().Get("user").(models.User)
	var roles []models.Role
	database.Db.Find(&user).Related(&roles)

	ctx.JSON(roles)
}

func Get(ctx iris.Context) {
	ID, err := ctx.Params().GetInt("id")

	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	user := ctx.Values().Get("user").(models.User)
	var role models.Role

	database.Db.Where("ID = ?", ID).First(&role)

	if role.ID == 0 {
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	if role.UserID != user.ID {
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	ctx.JSON(role)
}

func Create(ctx iris.Context) {
	user := ctx.Values().Get("user").(models.User)
	var role models.Role

	err := ctx.ReadJSON(&role)

	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}

	role.UserID = user.ID

	if role.ID > 0 {
		database.Db.Save(&role)
	} else {
		database.Db.Create(&role)
	}

	ctx.JSON(role)
}

func Delete(ctx iris.Context) {
	ID, err := ctx.Params().GetInt("id")

	if err != nil {
		ctx.JSON(iris.Map{"message": "error while trying to parse id parameter," +
			"this will never happen if :int is being used because if it's not integer it will fire Not Found automatically."})
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	var role models.Role
	database.Db.First(&role, ID)

	if role.ID > 0 {
		database.Db.Delete(&role)
	}
}
