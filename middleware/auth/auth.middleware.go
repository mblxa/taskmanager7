package authMiddleware

import (
	"bitbucket.org/mblxa/TaskManager7/database"
	"bitbucket.org/mblxa/TaskManager7/models"
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/kataras/iris"
)

func CheckToken(ctx iris.Context) {
	token := ctx.GetHeader("token")
	if token == "" {
		ctx.StatusCode(iris.StatusUnauthorized)
		return
	}

	user, err := findUserByToken(token, database.Db)
	if err != nil {
		ctx.StatusCode(iris.StatusUnauthorized)
		return
	}

	ctx.Values().Set("user", user)
	ctx.Next()
}

func findUserByToken(token string, db *gorm.DB) (models.User, error) {
	var user models.User

	db.Where("token = ?", token).First(&user)

	if user.ID == 0 {
		return user, errors.New("user not found")
	}

	return user, db.Error
}
