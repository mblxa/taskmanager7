package userController

import (
	"bitbucket.org/mblxa/TaskManager7/database"
	"bitbucket.org/mblxa/TaskManager7/models"
	"bitbucket.org/mblxa/TaskManager7/passwords"
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/kataras/iris"
)

//Type used to store user credentials for auth
type AuthJson struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

//Type used for valid auth response
type AuthResponse struct {
	Token string `json:"token"`
}

func Auth(ctx iris.Context) {
	var authData AuthJson
	err := ctx.ReadJSON(&authData)

	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}

	token, err := auth(authData, database.Db)

	if err != nil {
		ctx.StatusCode(iris.StatusUnauthorized)
		ctx.WriteString(err.Error())
		return
	}

	ctx.JSON(AuthResponse{token})
}

//Validates credentials and generates new token if credentials are correct
func auth(authData AuthJson, db *gorm.DB) (string, error) {
	user, err := findUserByEmail(authData.Email, db)

	if err != nil {
		return "", err
	}

	auth := passwords.CheckPasswordHash(authData.Password, user.Password)

	if auth {
		if user.Token == "" {
			user.GenerateToken()
		}
		db.Save(&user)
		return user.Token, err
	}

	return "", errors.New("not match")
}

func findUserByEmail(email string, db *gorm.DB) (models.User, error) {
	var user models.User
	db.Where("email = ?", email).First(&user)

	return user, db.Error
}

func Create(ctx iris.Context) {
	var user models.User

	err := ctx.ReadJSON(&user)

	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}

	database.Db.Where("email = ?", user.Email).First(&user)

	if user.ID != 0 {
		ctx.StatusCode(iris.StatusUnauthorized)
		ctx.WriteString("User exists")
		return
	}

	user.GenerateToken()
	database.Db.Create(&user)

	ctx.JSON(AuthResponse{user.Token})
}
