package taskController

import (
	"bitbucket.org/mblxa/TaskManager7/database"
	"bitbucket.org/mblxa/TaskManager7/models"
	"github.com/kataras/iris"
	"time"
)

func List(ctx iris.Context) {
	user := ctx.Values().Get("user").(models.User)
	status := ctx.FormValue("status")
	var tasks []models.Task

	switch status {
	case models.StatusInProgress, models.StatusComplete:
		database.Db.Find(&user).Order("Date ASC").
			Where("status = ?", status).
			Preload("Role").Related(&tasks)
	default:
		database.Db.Find(&user).Order("Date ASC").
			Preload("Role").Related(&tasks)
	}

	ctx.JSON(tasks)
}

func GetByDate(ctx iris.Context) {
	user := ctx.Values().Get("user").(models.User)
	var tasks []models.Task

	now := time.Now()
	now.AddDate(0, 0, 1)
	database.Db.Order("Date ASC").Preload("Role").
		Where("user_id = ? AND date < ? AND status = ?", user.ID, now.Format(time.RFC3339), models.StatusInProgress).
		Find(&tasks)

	ctx.JSON(tasks)
}

func ChangeStatus(ctx iris.Context) {
	ID, err := ctx.Params().GetInt("id")

	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	user := ctx.Values().Get("user").(models.User)
	var task models.Task

	database.Db.Where("ID = ?", ID).First(&task)

	if task.ID == 0 {
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	if task.UserID != user.ID {
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	type Status struct {
		Status int
	}

	var status Status

	errRead := ctx.ReadJSON(&status)
	if errRead != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}

	task.Status = status.Status
	database.Db.Save(&task)

	ctx.JSON(task)
}

func Get(ctx iris.Context) {
	ID, err := ctx.Params().GetInt("id")

	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	user := ctx.Values().Get("user").(models.User)
	var task models.Task

	database.Db.Where("ID = ?", ID).First(&task)

	if task.ID == 0 {
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	if task.UserID != user.ID {
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}
	ctx.JSON(task)
}

func Create(ctx iris.Context) {
	user := ctx.Values().Get("user").(models.User)
	var task models.Task

	err := ctx.ReadJSON(&task)

	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}

	task.UserID = user.ID

	if task.ID > 0 {
		database.Db.Save(&task)
	} else {
		database.Db.Create(&task)
	}

	ctx.JSON(task)
}

func Delete(ctx iris.Context) {
	ID, err := ctx.Params().GetInt("id")

	if err != nil {
		ctx.JSON(iris.Map{"message": "error while trying to parse id parameter," +
			"this will never happen if :int is being used because if it's not integer it will fire Not Found automatically."})
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	var task models.Task
	database.Db.First(&task, ID)

	if task.ID > 0 {
		database.Db.Delete(&task)
	}
}
