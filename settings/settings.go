package settings

import (
	"os"
	"strconv"
)

type settings struct {
	Server server
}

type server struct {
	IP          string
	PORT        string
	CERTIFICATE string
	KEYFILE     string
	HTTPS       bool
}

var Settings *settings

func init() {
	Settings = new(settings)
	Settings.Server = initServerSettings()
}

func initServerSettings() server {
	server := server{}

	server.IP = getOsVariable("SERVER_IP", "")
	server.PORT = getOsVariable("SERVER_PORT", "5002")
	server.CERTIFICATE = getOsVariable("SERVER_CERTIFICATE", "")
	server.KEYFILE = getOsVariable("SERVER_KEYFILE", "")
	server.HTTPS, _ = strconv.ParseBool(getOsVariable("HTTPS", "false"))

	return server
}

func getOsVariable(name string, defaultValue string) string {
	variable := os.Getenv(name)

	if variable == "" {
		return defaultValue
	}

	return variable
}

func (settings *settings) GetServerSettings() server {
	return settings.Server
}
