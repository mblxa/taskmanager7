package database

import (
	"bitbucket.org/mblxa/TaskManager7/models"
	"github.com/jinzhu/gorm"
)

var Db *gorm.DB

func Init() *gorm.DB {
	var err error
	Db, err = gorm.Open("sqlite3", "./db/tasks.db")
	if err != nil {
		panic("failed to connect database")
	}

	// Migrate the schema
	Db.AutoMigrate(&models.User{})
	Db.AutoMigrate(&models.Task{})
	Db.AutoMigrate(&models.Role{})

	var users []models.User
	Db.Find(&users)

	if len(users) == 0 {
		userM := models.User{Name: "Anton", Email: "admin", Password: "123"}
		Db.Create(&userM)
	}

	return Db
}
